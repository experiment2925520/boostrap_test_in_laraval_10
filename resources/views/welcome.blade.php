<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div class="XX-Large d-flex justify-content-center align-items-center min-vh-100">
        <h3>This Blade file styled using Boostrap 5</h3>
    </div>
</body>
</html>