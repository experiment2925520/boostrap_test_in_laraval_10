## Create Laravel 10 project
```
composer create-project laravel/laravel:^10.0 app-name
```

## Run app and confirm not error
```
cd example-app
 
php artisan serve
```

<br>



## install laravel/ui package.
```
composer require laravel/ui
```

## Add Bootstrap library using laravel/ui
```
php artisan ui bootstrap
```

## Install Bootstrap Icon
```
npm install bootstrap-icons
```

## import icon css on resources\sass\app.scss (IF NOT EXISTS)
```
/* Fonts */ 
@import url('https://fonts.bunny.net/css?family=Nunito'); 

/* Variables */ 
@import 'variables'; 

/* Bootstrap */ 
@import 'bootstrap/scss/bootstrap'; 
@import 'bootstrap-icons/font/bootstrap-icons.css';
```

## Build CSS and JS File.
```
npm install 
npm run build
```

## Add styles using bootstrap
It is required to add the following code in the blade file to use the bootstrap styles.
`@vite(['resources/sass/app.scss', 'resources/js/app.js'])`
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>


    @vite(['resources/sass/app.scss', 'resources/js/app.js'])


</head>
<body>
    <div class="XX-Large d-flex justify-content-center align-items-center min-vh-100">
        <h3>This Blade file styled using Boostrap 5</h3>
    </div>
</body>
</html>
```

## Finally, Run the app
```
php artisan serve
```